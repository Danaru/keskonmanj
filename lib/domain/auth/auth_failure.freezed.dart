// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'auth_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AuthFailureTearOff {
  const _$AuthFailureTearOff();

  _Unknown unknown() {
    return const _Unknown();
  }

  _ServerError serverError(String? error) {
    return _ServerError(
      error,
    );
  }

  _EmailAlreadyExists emailAlreadyExists() {
    return const _EmailAlreadyExists();
  }

  _UsernameAlreadyExists usernameAlreadyExists() {
    return const _UsernameAlreadyExists();
  }

  _BadCredentials badCredentials() {
    return const _BadCredentials();
  }
}

/// @nodoc
const $AuthFailure = _$AuthFailureTearOff();

/// @nodoc
mixin _$AuthFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unknown,
    required TResult Function(String? error) serverError,
    required TResult Function() emailAlreadyExists,
    required TResult Function() usernameAlreadyExists,
    required TResult Function() badCredentials,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unknown,
    TResult Function(String? error)? serverError,
    TResult Function()? emailAlreadyExists,
    TResult Function()? usernameAlreadyExists,
    TResult Function()? badCredentials,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_EmailAlreadyExists value) emailAlreadyExists,
    required TResult Function(_UsernameAlreadyExists value)
        usernameAlreadyExists,
    required TResult Function(_BadCredentials value) badCredentials,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_ServerError value)? serverError,
    TResult Function(_EmailAlreadyExists value)? emailAlreadyExists,
    TResult Function(_UsernameAlreadyExists value)? usernameAlreadyExists,
    TResult Function(_BadCredentials value)? badCredentials,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthFailureCopyWith<$Res> {
  factory $AuthFailureCopyWith(
          AuthFailure value, $Res Function(AuthFailure) then) =
      _$AuthFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthFailureCopyWithImpl<$Res> implements $AuthFailureCopyWith<$Res> {
  _$AuthFailureCopyWithImpl(this._value, this._then);

  final AuthFailure _value;
  // ignore: unused_field
  final $Res Function(AuthFailure) _then;
}

/// @nodoc
abstract class _$UnknownCopyWith<$Res> {
  factory _$UnknownCopyWith(_Unknown value, $Res Function(_Unknown) then) =
      __$UnknownCopyWithImpl<$Res>;
}

/// @nodoc
class __$UnknownCopyWithImpl<$Res> extends _$AuthFailureCopyWithImpl<$Res>
    implements _$UnknownCopyWith<$Res> {
  __$UnknownCopyWithImpl(_Unknown _value, $Res Function(_Unknown) _then)
      : super(_value, (v) => _then(v as _Unknown));

  @override
  _Unknown get _value => super._value as _Unknown;
}

/// @nodoc

class _$_Unknown implements _Unknown {
  const _$_Unknown();

  @override
  String toString() {
    return 'AuthFailure.unknown()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Unknown);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unknown,
    required TResult Function(String? error) serverError,
    required TResult Function() emailAlreadyExists,
    required TResult Function() usernameAlreadyExists,
    required TResult Function() badCredentials,
  }) {
    return unknown();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unknown,
    TResult Function(String? error)? serverError,
    TResult Function()? emailAlreadyExists,
    TResult Function()? usernameAlreadyExists,
    TResult Function()? badCredentials,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_EmailAlreadyExists value) emailAlreadyExists,
    required TResult Function(_UsernameAlreadyExists value)
        usernameAlreadyExists,
    required TResult Function(_BadCredentials value) badCredentials,
  }) {
    return unknown(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_ServerError value)? serverError,
    TResult Function(_EmailAlreadyExists value)? emailAlreadyExists,
    TResult Function(_UsernameAlreadyExists value)? usernameAlreadyExists,
    TResult Function(_BadCredentials value)? badCredentials,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(this);
    }
    return orElse();
  }
}

abstract class _Unknown implements AuthFailure {
  const factory _Unknown() = _$_Unknown;
}

/// @nodoc
abstract class _$ServerErrorCopyWith<$Res> {
  factory _$ServerErrorCopyWith(
          _ServerError value, $Res Function(_ServerError) then) =
      __$ServerErrorCopyWithImpl<$Res>;
  $Res call({String? error});
}

/// @nodoc
class __$ServerErrorCopyWithImpl<$Res> extends _$AuthFailureCopyWithImpl<$Res>
    implements _$ServerErrorCopyWith<$Res> {
  __$ServerErrorCopyWithImpl(
      _ServerError _value, $Res Function(_ServerError) _then)
      : super(_value, (v) => _then(v as _ServerError));

  @override
  _ServerError get _value => super._value as _ServerError;

  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_ServerError(
      error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_ServerError implements _ServerError {
  const _$_ServerError(this.error);

  @override
  final String? error;

  @override
  String toString() {
    return 'AuthFailure.serverError(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ServerError &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @JsonKey(ignore: true)
  @override
  _$ServerErrorCopyWith<_ServerError> get copyWith =>
      __$ServerErrorCopyWithImpl<_ServerError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unknown,
    required TResult Function(String? error) serverError,
    required TResult Function() emailAlreadyExists,
    required TResult Function() usernameAlreadyExists,
    required TResult Function() badCredentials,
  }) {
    return serverError(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unknown,
    TResult Function(String? error)? serverError,
    TResult Function()? emailAlreadyExists,
    TResult Function()? usernameAlreadyExists,
    TResult Function()? badCredentials,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_EmailAlreadyExists value) emailAlreadyExists,
    required TResult Function(_UsernameAlreadyExists value)
        usernameAlreadyExists,
    required TResult Function(_BadCredentials value) badCredentials,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_ServerError value)? serverError,
    TResult Function(_EmailAlreadyExists value)? emailAlreadyExists,
    TResult Function(_UsernameAlreadyExists value)? usernameAlreadyExists,
    TResult Function(_BadCredentials value)? badCredentials,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class _ServerError implements AuthFailure {
  const factory _ServerError(String? error) = _$_ServerError;

  String? get error => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$ServerErrorCopyWith<_ServerError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$EmailAlreadyExistsCopyWith<$Res> {
  factory _$EmailAlreadyExistsCopyWith(
          _EmailAlreadyExists value, $Res Function(_EmailAlreadyExists) then) =
      __$EmailAlreadyExistsCopyWithImpl<$Res>;
}

/// @nodoc
class __$EmailAlreadyExistsCopyWithImpl<$Res>
    extends _$AuthFailureCopyWithImpl<$Res>
    implements _$EmailAlreadyExistsCopyWith<$Res> {
  __$EmailAlreadyExistsCopyWithImpl(
      _EmailAlreadyExists _value, $Res Function(_EmailAlreadyExists) _then)
      : super(_value, (v) => _then(v as _EmailAlreadyExists));

  @override
  _EmailAlreadyExists get _value => super._value as _EmailAlreadyExists;
}

/// @nodoc

class _$_EmailAlreadyExists implements _EmailAlreadyExists {
  const _$_EmailAlreadyExists();

  @override
  String toString() {
    return 'AuthFailure.emailAlreadyExists()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _EmailAlreadyExists);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unknown,
    required TResult Function(String? error) serverError,
    required TResult Function() emailAlreadyExists,
    required TResult Function() usernameAlreadyExists,
    required TResult Function() badCredentials,
  }) {
    return emailAlreadyExists();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unknown,
    TResult Function(String? error)? serverError,
    TResult Function()? emailAlreadyExists,
    TResult Function()? usernameAlreadyExists,
    TResult Function()? badCredentials,
    required TResult orElse(),
  }) {
    if (emailAlreadyExists != null) {
      return emailAlreadyExists();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_EmailAlreadyExists value) emailAlreadyExists,
    required TResult Function(_UsernameAlreadyExists value)
        usernameAlreadyExists,
    required TResult Function(_BadCredentials value) badCredentials,
  }) {
    return emailAlreadyExists(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_ServerError value)? serverError,
    TResult Function(_EmailAlreadyExists value)? emailAlreadyExists,
    TResult Function(_UsernameAlreadyExists value)? usernameAlreadyExists,
    TResult Function(_BadCredentials value)? badCredentials,
    required TResult orElse(),
  }) {
    if (emailAlreadyExists != null) {
      return emailAlreadyExists(this);
    }
    return orElse();
  }
}

abstract class _EmailAlreadyExists implements AuthFailure {
  const factory _EmailAlreadyExists() = _$_EmailAlreadyExists;
}

/// @nodoc
abstract class _$UsernameAlreadyExistsCopyWith<$Res> {
  factory _$UsernameAlreadyExistsCopyWith(_UsernameAlreadyExists value,
          $Res Function(_UsernameAlreadyExists) then) =
      __$UsernameAlreadyExistsCopyWithImpl<$Res>;
}

/// @nodoc
class __$UsernameAlreadyExistsCopyWithImpl<$Res>
    extends _$AuthFailureCopyWithImpl<$Res>
    implements _$UsernameAlreadyExistsCopyWith<$Res> {
  __$UsernameAlreadyExistsCopyWithImpl(_UsernameAlreadyExists _value,
      $Res Function(_UsernameAlreadyExists) _then)
      : super(_value, (v) => _then(v as _UsernameAlreadyExists));

  @override
  _UsernameAlreadyExists get _value => super._value as _UsernameAlreadyExists;
}

/// @nodoc

class _$_UsernameAlreadyExists implements _UsernameAlreadyExists {
  const _$_UsernameAlreadyExists();

  @override
  String toString() {
    return 'AuthFailure.usernameAlreadyExists()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _UsernameAlreadyExists);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unknown,
    required TResult Function(String? error) serverError,
    required TResult Function() emailAlreadyExists,
    required TResult Function() usernameAlreadyExists,
    required TResult Function() badCredentials,
  }) {
    return usernameAlreadyExists();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unknown,
    TResult Function(String? error)? serverError,
    TResult Function()? emailAlreadyExists,
    TResult Function()? usernameAlreadyExists,
    TResult Function()? badCredentials,
    required TResult orElse(),
  }) {
    if (usernameAlreadyExists != null) {
      return usernameAlreadyExists();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_EmailAlreadyExists value) emailAlreadyExists,
    required TResult Function(_UsernameAlreadyExists value)
        usernameAlreadyExists,
    required TResult Function(_BadCredentials value) badCredentials,
  }) {
    return usernameAlreadyExists(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_ServerError value)? serverError,
    TResult Function(_EmailAlreadyExists value)? emailAlreadyExists,
    TResult Function(_UsernameAlreadyExists value)? usernameAlreadyExists,
    TResult Function(_BadCredentials value)? badCredentials,
    required TResult orElse(),
  }) {
    if (usernameAlreadyExists != null) {
      return usernameAlreadyExists(this);
    }
    return orElse();
  }
}

abstract class _UsernameAlreadyExists implements AuthFailure {
  const factory _UsernameAlreadyExists() = _$_UsernameAlreadyExists;
}

/// @nodoc
abstract class _$BadCredentialsCopyWith<$Res> {
  factory _$BadCredentialsCopyWith(
          _BadCredentials value, $Res Function(_BadCredentials) then) =
      __$BadCredentialsCopyWithImpl<$Res>;
}

/// @nodoc
class __$BadCredentialsCopyWithImpl<$Res>
    extends _$AuthFailureCopyWithImpl<$Res>
    implements _$BadCredentialsCopyWith<$Res> {
  __$BadCredentialsCopyWithImpl(
      _BadCredentials _value, $Res Function(_BadCredentials) _then)
      : super(_value, (v) => _then(v as _BadCredentials));

  @override
  _BadCredentials get _value => super._value as _BadCredentials;
}

/// @nodoc

class _$_BadCredentials implements _BadCredentials {
  const _$_BadCredentials();

  @override
  String toString() {
    return 'AuthFailure.badCredentials()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _BadCredentials);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() unknown,
    required TResult Function(String? error) serverError,
    required TResult Function() emailAlreadyExists,
    required TResult Function() usernameAlreadyExists,
    required TResult Function() badCredentials,
  }) {
    return badCredentials();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? unknown,
    TResult Function(String? error)? serverError,
    TResult Function()? emailAlreadyExists,
    TResult Function()? usernameAlreadyExists,
    TResult Function()? badCredentials,
    required TResult orElse(),
  }) {
    if (badCredentials != null) {
      return badCredentials();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Unknown value) unknown,
    required TResult Function(_ServerError value) serverError,
    required TResult Function(_EmailAlreadyExists value) emailAlreadyExists,
    required TResult Function(_UsernameAlreadyExists value)
        usernameAlreadyExists,
    required TResult Function(_BadCredentials value) badCredentials,
  }) {
    return badCredentials(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Unknown value)? unknown,
    TResult Function(_ServerError value)? serverError,
    TResult Function(_EmailAlreadyExists value)? emailAlreadyExists,
    TResult Function(_UsernameAlreadyExists value)? usernameAlreadyExists,
    TResult Function(_BadCredentials value)? badCredentials,
    required TResult orElse(),
  }) {
    if (badCredentials != null) {
      return badCredentials(this);
    }
    return orElse();
  }
}

abstract class _BadCredentials implements AuthFailure {
  const factory _BadCredentials() = _$_BadCredentials;
}
