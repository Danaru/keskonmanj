import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import '../core/parse_configuration.dart';
import 'package:retrofit/http.dart';
import 'package:retrofit/retrofit.dart';

import 'dtos.dart';

part 'parse_user_data_source.g.dart';

@lazySingleton
@RestApi(baseUrl: "$PARSE_SERVER_URL")
abstract class ParseAuthRemoteDataSource {
  @factoryMethod
  factory ParseAuthRemoteDataSource(Dio dio) = _ParseAuthRemoteDataSource;

  @POST("/users")
  Future<HttpResponse<UserDTO>> singUp(@Body() UserDTO user);

  @POST("/login")
  Future<HttpResponse<UserDTO>> login(@Body() UserDTO userDTO);
}
