import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'register_account_state.dart';
import '../../../domain/auth/i_auth_facade.dart';

@injectable
class RegisterAccountCubit extends Cubit<RegisterAccountState> {
  final IAuthFacade _authFacade;

  RegisterAccountCubit(this._authFacade)
      : super(RegisterAccountState.initial());

  Future<void> registerAccount(
      {required String email,
      required String password,
      required String username}) async {
    emit(RegisterAccountState.registering());
    final failOrRegistered = await _authFacade.registerAccount(
        username: username, password: password, email: email);

    failOrRegistered.fold(
      (failed) => emit(RegisterAccountState.failed(failed)),
      (r) => emit(RegisterAccountState.succeed()),
    );
  }
}
