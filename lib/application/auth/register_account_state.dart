import 'package:freezed_annotation/freezed_annotation.dart';
import '../../../domain/auth/auth_failure.dart';

part 'register_account_state.freezed.dart';

@freezed
class RegisterAccountState with _$RegisterAccountState {
  const factory RegisterAccountState.initial() = _Initial;
  const factory RegisterAccountState.registering() = _Registering;
  const factory RegisterAccountState.failed(AuthFailure failure) = _Failed;
  const factory RegisterAccountState.succeed() = _Succeed;
}
