class Setup
  def initialize(base_path, dev)
    @base_path = base_path
    @dev = dev
  end

  def dev?
    @dev
  end

  def application_id_setup_path
    "#{@base_path}/pubspec.yaml"
  end

  def launcher_icons_setup_path
    "#{@base_path}/pubspec.yaml"
  end

  def keys_password
    @keys_password ||= ENV['KEYS_PASSWORD'] || UI.password("Mot de passe des certificats ?")
  end
  
  def build_number
    @build_number ||= ENV['CI_PIPELINE_IID'] || ENV['BUILD_NUMBER'] || UI.input("Numéro de build ?")
  end

  def package_name_release
    "#{get_setup('name')}-Release"
  end

  def package_name_profile
    "#{get_setup('name')}-Profile"
  end

  def changelog_path
    "#{@base_path}/CHANGELOG.md"
  end

  #############
  #    iOS
  #############
  def apple_user_id
    @apple_user_id ||= ENV['APPLE_USER_ID'] || UI.input("Apple user ID ?")
  end
  
  def apple_team_id
    get_setup('apple_team_id')
  end
  
  def apple_itc_team_id
    get_setup('apple_itc_team_id')
  end
  
  def apple_application_id
    get_setup('apple_id')
  end
  
  def apple_app_password
    @apple_app_password ||= ENV['APPLE_APP_PASSWORD'] || UI.password("Apple application specific password ?")
  end
  
  def apple_release_cert_path
    "#{@base_path}/deploy/appstore.p12"
  end

  def apple_dev_cert_path
    "#{@base_path}/deploy/dev.p12"
  end

  def apple_dev_provisioning_profile_path
    "#{@base_path}/deploy/dev.mobileprovision"
  end

  def apple_release_provisioning_profile_path
    "#{@base_path}/deploy/appstore.mobileprovision"
  end

  def ipa_path_release
    "#{@base_path}/#{package_name_release}.ipa"
  end

  def ipa_path_profile
    "#{@base_path}/#{package_name_profile}.ipa"
  end

  #############
  #  Android
  #############
  def json_key_path
    "#{@base_path}/deploy/api.json"
  end

  def aab_path_release
    "#{@base_path}/#{package_name_release}.aab"
  end

  def aab_path_profile
    "#{@base_path}/#{package_name_profile}.aab"
  end
  
  def apk_path_release
    "#{@base_path}/#{package_name_release}.apk"
  end

  def apk_path_profile
    "#{@base_path}/#{package_name_profile}.apk"
  end
  
  def android_release_cert_path
    "#{@base_path}/deploy/upload.keystore"
  end
  
  private
  def get_setup(key)
    @setup_data ||= YAML.load_file('pubspec.yaml')
    if @setup_data[key] == nil 
      UI.abort_with_message! "Missing `#{key}` in `setup.yaml`"
    end
    return @setup_data[key]
  end
end

def apply_setup(setup)
  Dir.chdir('..') do
    UI.header "Paramétrage du projet"

    # UI.message "Mise en place des fichiers de configuration firebase"
    # FileUtils.cp_r("#{scheme_path}/google-services.json", "./android/app/", remove_destination: true)
    # FileUtils.cp_r("#{scheme_path}/GoogleService-Info.plist", "./ios/Runner/", remove_destination: true)
    
    sh("#{FLUTTER_BIN} pub get")
    
    if OS.mac?
        # Dossier dans lequel on va stocker les assets
        # le temps d'exécuter les plugins de config.
        apply_code_signing_settings(
          dev: setup.dev?,
          cert_path: setup.dev? ? setup.apple_dev_cert_path : setup.apple_release_cert_path,
          cert_password: setup.keys_password,
          apple_team_id: setup.apple_team_id,
          provisioning_profile_path: setup.dev? ? setup.apple_dev_provisioning_profile_path : setup.apple_release_provisioning_profile_path)
    end
  end
end

def clean_setup
  UI.header "Nettoyage du paramétrage projet"
  reset_git_repo(force:true, files: ["./ios/", "./android/"])
end

