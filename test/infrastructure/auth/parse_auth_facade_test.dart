import 'package:flutter_test/flutter_test.dart';
import 'package:keskonmanj/domain/auth/auth_failure.dart';

import 'package:keskonmanj/infrastructure/auth/parse_auth_facade.dart';

import 'package:keskonmanj/infrastructure/auth/parse_user_data_source.dart';
import 'package:keskonmanj/infrastructure/core/parse_configuration.dart';

import 'package:dio/dio.dart';

main() {
  // Based on parse error doc
  // https://docs.parseplatform.org/rest/guide/#user-related-errors
  group("mapErrorToFailure", () {
    DioError _buildError(
            int statusCode, int parseCode, String parseError, String path) =>
        DioError(
            requestOptions: RequestOptions(path: "$PARSE_SERVER_URL$path"),
            response: Response<dynamic>(
                requestOptions: RequestOptions(path: "$PARSE_SERVER_URL$path"),
                statusCode: statusCode,
                data: <String, dynamic>{
                  "code": parseCode,
                  "error": parseError
                }));

    test("Should return AuthFailure.unknown() if parse code 200", () async {
      final dioError =
          _buildError(400, 200, "bad or missing username", "/users");

      final expected = AuthFailure.unknown();

      final actual = ParseAuthFacade(ParseAuthRemoteDataSource(Dio()))
          .mapErrorToFailure(dioError);

      expect(actual, expected);
    });

    test("Should return AuthFailure.unknown() if parse code 201", () async {
      final dioError = _buildError(400, 201, "password is required", "/users");

      final expected = AuthFailure.unknown();

      final actual = ParseAuthFacade(ParseAuthRemoteDataSource(Dio()))
          .mapErrorToFailure(dioError);

      expect(actual, expected);
    });

    test("Should return AuthFailure.usernameAlreadyExists() if parse code 202",
        () async {
      final dioError = _buildError(
          400, 202, "Account already exists for this username.", "/users");

      final expected = AuthFailure.usernameAlreadyExists();

      final actual = ParseAuthFacade(ParseAuthRemoteDataSource(Dio()))
          .mapErrorToFailure(dioError);

      expect(actual, expected);
    });

    test("Should return AuthFailure.emailAlreadyExsist() if parse code 203",
        () async {
      final dioError = _buildError(
          400, 203, "Account already exists for this email address.", "/users");

      final expected = AuthFailure.emailAlreadyExists();

      final actual = ParseAuthFacade(ParseAuthRemoteDataSource(Dio()))
          .mapErrorToFailure(dioError);

      expect(actual, expected);
    });

    test("Should return AuthFailure.badCredentials() if parse code 205",
        () async {
      final dioError = _buildError(
          400, 205, "Account already exists for this email address.", "/users");

      final expected = AuthFailure.badCredentials();

      final actual = ParseAuthFacade(ParseAuthRemoteDataSource(Dio()))
          .mapErrorToFailure(dioError);

      expect(actual, expected);
    });

    group("For generic 101 code", () {
      test(
          "Should return AuthFailure.badCredentials() if parse code 101 and message is Invalid username/password.",
          () async {
        final dioError =
            _buildError(400, 101, "Invalid username/password.", "/login");

        final expected = AuthFailure.badCredentials();

        final actual = ParseAuthFacade(ParseAuthRemoteDataSource(Dio()))
            .mapErrorToFailure(dioError);

        expect(actual, expected);
      });
    });
  });
}
