import '../../../domain/auth/auth_failure.dart';

extension AuthFailureMessage on AuthFailure {
  String get message => maybeMap(
        emailAlreadyExists: (_) =>
            'Un compte existe déjà avec cette adrese email.',
        usernameAlreadyExists: (_) =>
            "Un compte existe déjà avec ce nom d'utilisateur",
        serverError: (_) =>
            "Une erreur serveur est survenue, veuillez retenter votre chance plus tard.",
        badCredentials: (_) => "Nom d'utilisateur ou mot de passe incorrect.",
        unknown: (_) => "Une erreur inconnue est survenue.",
        orElse: () => toString(),
      );
}
