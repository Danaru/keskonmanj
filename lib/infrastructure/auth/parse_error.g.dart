// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'parse_error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ParseError _$_$_ParseErrorFromJson(Map<String, dynamic> json) {
  return _$_ParseError(
    code: json['code'] as int?,
    error: json['error'] as String?,
  );
}

Map<String, dynamic> _$_$_ParseErrorToJson(_$_ParseError instance) =>
    <String, dynamic>{
      'code': instance.code,
      'error': instance.error,
    };
