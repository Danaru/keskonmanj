import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../application/auth/register_account_cubit.dart';
import '../../application/auth/register_account_state.dart';
import '../../domain/core/validators.dart';
import '../../injection.dart';
import '../core/widgets/dialogs.dart';
import '../core/widgets/kkm_text_form_fields.dart';
import 'package:auto_route/auto_route.dart';
import '../core/router.gr.dart';
import '../core/extensions/auth_failure_extension.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: BlocProvider<RegisterAccountCubit>(
        create: (context) => getIt<RegisterAccountCubit>(),
        child: Container(
          child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: _RegisterFormWidget()),
        ),
      ),
    );
  }
}

class _RegisterFormWidget extends StatefulWidget {
  @override
  _RegisterFormWidgetState createState() => _RegisterFormWidgetState();
}

class _RegisterFormWidgetState extends State<_RegisterFormWidget> {
  static const DIALOG_KEY = ValueKey('REGISTER_DIALOG');
  final _emailCtrl = TextEditingController();
  final _passwordCtrl = TextEditingController();
  final _usernameCtrl = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _emailCtrl.dispose();
    _passwordCtrl.dispose();
    _usernameCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterAccountCubit, RegisterAccountState>(
      listener: (context, state) {
        state.maybeMap(
            registering: (_) {
              KKMDialog.show(
                  context, KKMDialog.waiting(title: "Inscription en cours"),
                  key: DIALOG_KEY);
            },
            succeed: (_) async {
              KKMDialog.hide(context, DIALOG_KEY);
              context.router.pushAndPopUntil(
                BlankScreenRoute(),
                predicate: (route) => false,
              );
            },
            failed: (failedState) {
              KKMDialog.hide(context, DIALOG_KEY);
              KKMDialog.show(
                  context,
                  KKMDialog.error(
                    errorMessage: failedState.failure.message,
                    key: DIALOG_KEY,
                  ),
                  key: DIALOG_KEY);
            },
            orElse: () {});
      },
      child: Center(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Inscription"),
                KKMTextFormField(
                  controller: _usernameCtrl,
                  validator: GenericValidators.mandatory,
                  autovalidatemode: AutovalidateMode.onUserInteraction,
                  labelText: "nom d'utilisateur",
                ),
                KKMTextFormField.email(
                  controller: _emailCtrl,
                ),
                KKMTextFormField.password(
                  controller: _passwordCtrl,
                  validateLength: true,
                ),
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        context.read<RegisterAccountCubit>().registerAccount(
                            email: _emailCtrl.text,
                            password: _passwordCtrl.text,
                            username: _usernameCtrl.text);
                      }
                    },
                    child: Text("Inscription"))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
