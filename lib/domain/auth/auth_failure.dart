import 'package:freezed_annotation/freezed_annotation.dart';
part 'auth_failure.freezed.dart';

@freezed
class AuthFailure with _$AuthFailure {
  const factory AuthFailure.unknown() = _Unknown;
  const factory AuthFailure.serverError(String? error) = _ServerError;
  const factory AuthFailure.emailAlreadyExists() = _EmailAlreadyExists;
  const factory AuthFailure.usernameAlreadyExists() = _UsernameAlreadyExists;
  const factory AuthFailure.badCredentials() = _BadCredentials;
}
