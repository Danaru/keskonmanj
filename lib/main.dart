import 'package:flutter/material.dart';

import 'injection.dart';
import 'presentation/core/router.gr.dart';

void main() async {
  configureDependencies();
  runApp(KKMApp());
}

class KKMApp extends StatelessWidget {
  final _appRouter = KKMRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: _appRouter.defaultRouteParser(),
      routerDelegate: _appRouter.delegate(),
      title: 'KesKonManj',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
