import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../application/auth/login_account_cubit.dart';
import '../../application/auth/login_account_state.dart';
import '../../domain/core/validators.dart';
import '../../injection.dart';
import '../core/widgets/dialogs.dart';
import '../core/widgets/kkm_text_form_fields.dart';
import '../core/router.gr.dart';
import '../core/extensions/auth_failure_extension.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("KesKonManj (WIP)"),
              SizedBox(
                height: 20,
              ),
              BlocProvider<LoginAccountCubit>(
                  create: (context) => getIt<LoginAccountCubit>(),
                  child: _LoginForm()),
            ],
          ),
        ),
      ),
    );
  }
}

class _LoginForm extends StatefulWidget {
  @override
  __LoginFormState createState() => __LoginFormState();
}

class __LoginFormState extends State<_LoginForm> {
  static const DIALOG_KEY = ValueKey('LOGIN_DIALOG');
  final _formKey = GlobalKey<FormState>();
  final _usernameNode = FocusNode();
  final _passNode = FocusNode();
  final _usernameCtrl = TextEditingController();
  final _passwordCtrl = TextEditingController();

  @override
  void dispose() {
    _usernameCtrl.dispose();
    _passwordCtrl.dispose();
    _usernameNode.dispose();
    _passNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginAccountCubit, LoginAccountState>(
      listener: (context, state) {
        state.maybeMap(
          loggingIn: (_) {
            KKMDialog.show(
                context, KKMDialog.waiting(title: "Inscription en cours"),
                key: DIALOG_KEY);
          },
          succeed: (_) {
            KKMDialog.hide(context, DIALOG_KEY);
            context.router.replace(
              BlankScreenRoute(),
            );
          },
          failed: (failedState) {
            KKMDialog.hide(context, DIALOG_KEY);
            KKMDialog.show(
                context,
                KKMDialog.error(
                  errorMessage: failedState.failure.message,
                  key: DIALOG_KEY,
                ),
                key: DIALOG_KEY);
          },
          orElse: () {},
        );
      },
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            KKMTextFormField(
              controller: _usernameCtrl,
              focusNode: _usernameNode,
              nextFocusNode: _passNode,
              autocorrect: false,
              labelText: "Nom d'utilisateur",
              hintText: "Nom d'utilisateur",
              validator: GenericValidators.mandatory,
            ),
            KKMTextFormField.password(
              controller: _passwordCtrl,
              focusNode: _passNode,
            ),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  context
                      .read<LoginAccountCubit>()
                      .login(_usernameCtrl.text, _passwordCtrl.text);
                }
              },
              child: Text("Connexion"),
            ),
            TextButton(
              onPressed: () {
                context.router.push(RegisterScreenRoute());
              },
              child: Text("Créer un compte"),
            )
          ],
        ),
      ),
    );
  }
}
