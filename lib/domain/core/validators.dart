typedef FieldValidator<T> = String? Function(T?);

class GenericValidators {
  static String? mandatory(dynamic value) {
    if (value is String && value.isEmpty) {
      return 'Ce champ est obligatoire';
    }
    return value == null ? 'Ce champ est obligatoire' : null;
  }

  static FieldValidator<T> aggregate<T>(List<FieldValidator<T>> validators) {
    return (T? value) => validators
        .map((validator) => validator(value))
        .firstWhere((error) => error != null, orElse: () => null);
  }
}

class PasswordValidators {
  static String? minLength(String? input) {
    if (input == null || input.length < 6) {
      return 'Ce mot de passe est trop court.';
    }
    return null;
  }
}

class EmailValidators {
  static String? validateFormat(String? input) {
    return !_isValidEmailFormat(input)
        ? 'Le format ne correspond pas a une adresse email'
        : null;
  }

  static bool _isValidEmailFormat(String? input) {
    const regex =
        r"""(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""";
    if (input == null) {
      return false;
    } else if (RegExp(regex).hasMatch(input)) {
      return true;
    } else {
      return false;
    }
  }
}
