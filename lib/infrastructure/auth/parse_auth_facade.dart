import 'package:injectable/injectable.dart';
import '../../domain/auth/auth_failure.dart';
import 'package:dartz/dartz.dart';
import '../../domain/auth/i_auth_facade.dart';
import 'dtos.dart';
import 'package:dio/dio.dart';

import 'parse_error.dart';
import 'parse_user_data_source.dart';

@LazySingleton(as: IAuthFacade)
class ParseAuthFacade implements IAuthFacade {
  final ParseAuthRemoteDataSource _authRemoteDataSource;

  ParseAuthFacade(this._authRemoteDataSource);
  @override
  Future<Either<AuthFailure, Unit>> registerAccount(
      {required String email,
      required String username,
      required String password}) async {
    try {
      await _authRemoteDataSource.singUp(UserDTO.forSignUp(
          username: username, password: password, email: email));

      //TODO: call localRepo to save user
      return right(unit);
    } on DioError catch (dioError) {
      return left(mapErrorToFailure(dioError));
    } catch (unknown) {
      return left(AuthFailure.unknown());
    }
  }

  @override
  Future<Either<AuthFailure, Unit>> login(
      String username, String password) async {
    try {
      final loginResponse = await _authRemoteDataSource
          .login(UserDTO.forLogin(username: username, password: password));
      //TODO: call localRepo to save user
      return right(unit);
    } on DioError catch (dioError) {
      return left(mapErrorToFailure(dioError));
    } catch (unknown) {
      return left(AuthFailure.unknown());
    }
  }

  AuthFailure mapErrorToFailure(DioError dioError) {
    if (dioError.response?.statusCode == 500) {
      return AuthFailure.serverError(dioError.response?.statusMessage);
    }

    final ParseError parseError = ParseError.fromJson(dioError.response?.data);

    switch (parseError.code) {
      case 101:
        return _mapForCode101(parseError);
      case 202:
        return AuthFailure.usernameAlreadyExists();
      case 203:
        return AuthFailure.emailAlreadyExists();
      case 205:
        return AuthFailure.badCredentials();
      default:
        return AuthFailure.unknown();
    }
  }

  AuthFailure _mapForCode101(ParseError error) {
    switch (error.error) {
      case "Invalid username/password.":
        return AuthFailure.badCredentials();
      default:
        return AuthFailure.unknown();
    }
  }
}
