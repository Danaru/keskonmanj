import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'dtos.freezed.dart';
part 'dtos.g.dart';

@freezed
class UserDTO with _$UserDTO {
  const factory UserDTO({
    String? objectId,
    DateTime? createdAt,
    String? sessionToken,
    String? username,
    String? password,
    String? email,
  }) = _UserDTO;

  factory UserDTO.forSignUp(
          {required String username,
          required String password,
          required String email}) =>
      UserDTO(username: username, password: password, email: email);

  factory UserDTO.forLogin({
    required String username,
    required String password,
  }) =>
      UserDTO(username: username, password: password);

  factory UserDTO.fromJson(Map<String, dynamic> json) =>
      _$UserDTOFromJson(json);
}
