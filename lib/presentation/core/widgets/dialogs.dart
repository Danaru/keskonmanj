import 'dart:async';

import 'package:flutter/material.dart';

abstract class KKMDialog extends StatelessWidget {
  const KKMDialog({Key? key}) : super(key: key);

  static Future show(BuildContext context, Widget dialog,
      {Duration? duration, required Key key}) async {
    final result = await showDialog<void>(
        context: context,
        useRootNavigator: true,
        barrierDismissible: true,
        routeSettings: RouteSettings(name: key.toString()),
        builder: (_) {
          if (duration != null) {
            Timer(duration, () {
              hide(context, key);
            });
          }
          return dialog;
        });
    FocusScope.of(context).requestFocus(FocusNode());
    return result;
  }

  factory KKMDialog.waiting({Key? key, required String title}) = _WaitingDialog;
  factory KKMDialog.error({Key? key, required String errorMessage}) =
      _ErrorDialog;

  static void hide(BuildContext context, Key key) {
    Navigator.popUntil(
        context, (route) => route.settings.name != key.toString());
  }
}

class _WaitingDialog extends KKMDialog {
  final String title;

  const _WaitingDialog({required this.title, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Center(
        child: Card(
          child: Container(
            width: 220,
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(title, style: Theme.of(context).textTheme.headline6),
                SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: CircularProgressIndicator(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _ErrorDialog extends KKMDialog {
  final String errorMessage;

  const _ErrorDialog({required this.errorMessage, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Container(
          width: 240,
          padding: EdgeInsets.only(top: 20.0, bottom: 10, left: 20, right: 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text('Oups !', style: Theme.of(context).textTheme.headline6),
              SizedBox(height: 15),
              Text(errorMessage, style: Theme.of(context).textTheme.headline6),
              SizedBox(height: 15),
              Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                  style: TextButton.styleFrom(
                      textStyle:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                  onPressed: () => Navigator.pop(context),
                  child: Text('FERMER'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
