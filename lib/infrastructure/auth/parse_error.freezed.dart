// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'parse_error.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ParseError _$ParseErrorFromJson(Map<String, dynamic> json) {
  return _ParseError.fromJson(json);
}

/// @nodoc
class _$ParseErrorTearOff {
  const _$ParseErrorTearOff();

  _ParseError call({int? code, String? error}) {
    return _ParseError(
      code: code,
      error: error,
    );
  }

  ParseError fromJson(Map<String, Object> json) {
    return ParseError.fromJson(json);
  }
}

/// @nodoc
const $ParseError = _$ParseErrorTearOff();

/// @nodoc
mixin _$ParseError {
  int? get code => throw _privateConstructorUsedError;
  String? get error => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ParseErrorCopyWith<ParseError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ParseErrorCopyWith<$Res> {
  factory $ParseErrorCopyWith(
          ParseError value, $Res Function(ParseError) then) =
      _$ParseErrorCopyWithImpl<$Res>;
  $Res call({int? code, String? error});
}

/// @nodoc
class _$ParseErrorCopyWithImpl<$Res> implements $ParseErrorCopyWith<$Res> {
  _$ParseErrorCopyWithImpl(this._value, this._then);

  final ParseError _value;
  // ignore: unused_field
  final $Res Function(ParseError) _then;

  @override
  $Res call({
    Object? code = freezed,
    Object? error = freezed,
  }) {
    return _then(_value.copyWith(
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as int?,
      error: error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$ParseErrorCopyWith<$Res> implements $ParseErrorCopyWith<$Res> {
  factory _$ParseErrorCopyWith(
          _ParseError value, $Res Function(_ParseError) then) =
      __$ParseErrorCopyWithImpl<$Res>;
  @override
  $Res call({int? code, String? error});
}

/// @nodoc
class __$ParseErrorCopyWithImpl<$Res> extends _$ParseErrorCopyWithImpl<$Res>
    implements _$ParseErrorCopyWith<$Res> {
  __$ParseErrorCopyWithImpl(
      _ParseError _value, $Res Function(_ParseError) _then)
      : super(_value, (v) => _then(v as _ParseError));

  @override
  _ParseError get _value => super._value as _ParseError;

  @override
  $Res call({
    Object? code = freezed,
    Object? error = freezed,
  }) {
    return _then(_ParseError(
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as int?,
      error: error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ParseError implements _ParseError {
  const _$_ParseError({this.code, this.error});

  factory _$_ParseError.fromJson(Map<String, dynamic> json) =>
      _$_$_ParseErrorFromJson(json);

  @override
  final int? code;
  @override
  final String? error;

  @override
  String toString() {
    return 'ParseError(code: $code, error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ParseError &&
            (identical(other.code, code) ||
                const DeepCollectionEquality().equals(other.code, code)) &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(code) ^
      const DeepCollectionEquality().hash(error);

  @JsonKey(ignore: true)
  @override
  _$ParseErrorCopyWith<_ParseError> get copyWith =>
      __$ParseErrorCopyWithImpl<_ParseError>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ParseErrorToJson(this);
  }
}

abstract class _ParseError implements ParseError {
  const factory _ParseError({int? code, String? error}) = _$_ParseError;

  factory _ParseError.fromJson(Map<String, dynamic> json) =
      _$_ParseError.fromJson;

  @override
  int? get code => throw _privateConstructorUsedError;
  @override
  String? get error => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ParseErrorCopyWith<_ParseError> get copyWith =>
      throw _privateConstructorUsedError;
}
