import 'package:dartz/dartz.dart';

import 'auth_failure.dart';

abstract class IAuthFacade {
  Future<Either<AuthFailure, Unit>> registerAccount(
      {required String username,
      required String password,
      required String email});

  Future<Either<AuthFailure, Unit>> login(String username, String password);
}
