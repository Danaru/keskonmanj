// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'application/auth/login_account_cubit.dart' as _i7;
import 'application/auth/register_account_cubit.dart' as _i8;
import 'domain/auth/i_auth_facade.dart' as _i5;
import 'infrastructure/auth/parse_auth_facade.dart' as _i6;
import 'infrastructure/auth/parse_user_data_source.dart' as _i4;
import 'infrastructure/core/third_party_modules.dart'
    as _i9; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final thirdPartyModules = _$ThirdPartyModules();
  gh.lazySingleton<_i3.Dio>(() => thirdPartyModules.dio());
  gh.lazySingleton<_i4.ParseAuthRemoteDataSource>(
      () => _i4.ParseAuthRemoteDataSource(get<_i3.Dio>()));
  gh.lazySingleton<_i5.IAuthFacade>(
      () => _i6.ParseAuthFacade(get<_i4.ParseAuthRemoteDataSource>()));
  gh.factory<_i7.LoginAccountCubit>(
      () => _i7.LoginAccountCubit(get<_i5.IAuthFacade>()));
  gh.factory<_i8.RegisterAccountCubit>(
      () => _i8.RegisterAccountCubit(get<_i5.IAuthFacade>()));
  return get;
}

class _$ThirdPartyModules extends _i9.ThirdPartyModules {}
