abstract class IInitFacade {
  Future<void> initApp();
}
