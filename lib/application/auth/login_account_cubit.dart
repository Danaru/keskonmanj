import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import '../../domain/auth/i_auth_facade.dart';

import 'login_account_state.dart';

@injectable
class LoginAccountCubit extends Cubit<LoginAccountState> {
  final IAuthFacade _authFacade;
  LoginAccountCubit(this._authFacade) : super(LoginAccountState.initial());

  void login(String username, String password) async {
    emit(LoginAccountState.loggingIn());
    final failOrLogged = await _authFacade.login(username, password);

    failOrLogged.fold(
      (fail) => emit(LoginAccountState.failed(fail)),
      (r) => emit(LoginAccountState.succeed()),
    );
  }
}
