import 'package:freezed_annotation/freezed_annotation.dart';
import '../../domain/auth/auth_failure.dart';

part 'login_account_state.freezed.dart';

@freezed
class LoginAccountState with _$LoginAccountState {
  const factory LoginAccountState.initial() = _Initial;
  const factory LoginAccountState.loggingIn() = _LoggingIn;
  const factory LoginAccountState.failed(AuthFailure failure) = _Failed;
  const factory LoginAccountState.succeed() = _Succeed;
}
