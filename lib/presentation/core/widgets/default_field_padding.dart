import 'package:flutter/material.dart';

class DefaultFieldPadding extends StatelessWidget {
  final EdgeInsets? padding;
  final Widget child;

  const DefaultFieldPadding({
    Key? key,
    this.padding,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          padding ?? const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
      child: child,
    );
  }
}
