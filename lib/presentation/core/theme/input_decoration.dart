import 'package:flutter/material.dart';

class KKMInputDecoration {
  static final roundedBorder = OutlineInputBorder(
      borderSide: BorderSide(color: Colors.black54, width: 0.8),
      borderRadius: BorderRadius.all(Radius.circular(45)));

  static InputDecoration roundedTextInput({
    String? hintText,
    String? labelText,
    TextStyle? hintStyle,
    TextStyle? labelStyle,
  }) =>
      InputDecoration(
        hintText: hintText,
        labelText: labelText,
        hintStyle: hintStyle,
        labelStyle: labelStyle,
        border: roundedBorder,
        disabledBorder: roundedBorder,
        enabledBorder: roundedBorder,
        focusedBorder: roundedBorder,
        alignLabelWithHint: true,
        isDense: true,
        contentPadding: EdgeInsets.fromLTRB(25, 15, 12, 15),
      );
}
