import 'package:injectable/injectable.dart';
import 'package:dio/dio.dart';
import 'parse_configuration.dart';

@module
abstract class ThirdPartyModules {
  @lazySingleton
  Dio dio() =>
      Dio(BaseOptions(baseUrl: PARSE_SERVER_URL, headers: <String, dynamic>{
        "X-Parse-Application-Id": PARSE_APP_ID,
        "X-Parse-REST-API-Key": PARSE_REST_API_KEY,
      }));
}
