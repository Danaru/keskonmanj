// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dtos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserDTO _$_$_UserDTOFromJson(Map<String, dynamic> json) {
  return _$_UserDTO(
    objectId: json['objectId'] as String?,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    sessionToken: json['sessionToken'] as String?,
    username: json['username'] as String?,
    password: json['password'] as String?,
    email: json['email'] as String?,
  );
}

Map<String, dynamic> _$_$_UserDTOToJson(_$_UserDTO instance) =>
    <String, dynamic>{
      'objectId': instance.objectId,
      'createdAt': instance.createdAt?.toIso8601String(),
      'sessionToken': instance.sessionToken,
      'username': instance.username,
      'password': instance.password,
      'email': instance.email,
    };
