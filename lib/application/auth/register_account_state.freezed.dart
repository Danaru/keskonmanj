// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'register_account_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$RegisterAccountStateTearOff {
  const _$RegisterAccountStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _Registering registering() {
    return const _Registering();
  }

  _Failed failed(AuthFailure failure) {
    return _Failed(
      failure,
    );
  }

  _Succeed succeed() {
    return const _Succeed();
  }
}

/// @nodoc
const $RegisterAccountState = _$RegisterAccountStateTearOff();

/// @nodoc
mixin _$RegisterAccountState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() registering,
    required TResult Function(AuthFailure failure) failed,
    required TResult Function() succeed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? registering,
    TResult Function(AuthFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Registering value) registering,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Registering value)? registering,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterAccountStateCopyWith<$Res> {
  factory $RegisterAccountStateCopyWith(RegisterAccountState value,
          $Res Function(RegisterAccountState) then) =
      _$RegisterAccountStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$RegisterAccountStateCopyWithImpl<$Res>
    implements $RegisterAccountStateCopyWith<$Res> {
  _$RegisterAccountStateCopyWithImpl(this._value, this._then);

  final RegisterAccountState _value;
  // ignore: unused_field
  final $Res Function(RegisterAccountState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res>
    extends _$RegisterAccountStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'RegisterAccountState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() registering,
    required TResult Function(AuthFailure failure) failed,
    required TResult Function() succeed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? registering,
    TResult Function(AuthFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Registering value) registering,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Registering value)? registering,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements RegisterAccountState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$RegisteringCopyWith<$Res> {
  factory _$RegisteringCopyWith(
          _Registering value, $Res Function(_Registering) then) =
      __$RegisteringCopyWithImpl<$Res>;
}

/// @nodoc
class __$RegisteringCopyWithImpl<$Res>
    extends _$RegisterAccountStateCopyWithImpl<$Res>
    implements _$RegisteringCopyWith<$Res> {
  __$RegisteringCopyWithImpl(
      _Registering _value, $Res Function(_Registering) _then)
      : super(_value, (v) => _then(v as _Registering));

  @override
  _Registering get _value => super._value as _Registering;
}

/// @nodoc

class _$_Registering implements _Registering {
  const _$_Registering();

  @override
  String toString() {
    return 'RegisterAccountState.registering()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Registering);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() registering,
    required TResult Function(AuthFailure failure) failed,
    required TResult Function() succeed,
  }) {
    return registering();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? registering,
    TResult Function(AuthFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) {
    if (registering != null) {
      return registering();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Registering value) registering,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) {
    return registering(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Registering value)? registering,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) {
    if (registering != null) {
      return registering(this);
    }
    return orElse();
  }
}

abstract class _Registering implements RegisterAccountState {
  const factory _Registering() = _$_Registering;
}

/// @nodoc
abstract class _$FailedCopyWith<$Res> {
  factory _$FailedCopyWith(_Failed value, $Res Function(_Failed) then) =
      __$FailedCopyWithImpl<$Res>;
  $Res call({AuthFailure failure});

  $AuthFailureCopyWith<$Res> get failure;
}

/// @nodoc
class __$FailedCopyWithImpl<$Res>
    extends _$RegisterAccountStateCopyWithImpl<$Res>
    implements _$FailedCopyWith<$Res> {
  __$FailedCopyWithImpl(_Failed _value, $Res Function(_Failed) _then)
      : super(_value, (v) => _then(v as _Failed));

  @override
  _Failed get _value => super._value as _Failed;

  @override
  $Res call({
    Object? failure = freezed,
  }) {
    return _then(_Failed(
      failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as AuthFailure,
    ));
  }

  @override
  $AuthFailureCopyWith<$Res> get failure {
    return $AuthFailureCopyWith<$Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc

class _$_Failed implements _Failed {
  const _$_Failed(this.failure);

  @override
  final AuthFailure failure;

  @override
  String toString() {
    return 'RegisterAccountState.failed(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Failed &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @JsonKey(ignore: true)
  @override
  _$FailedCopyWith<_Failed> get copyWith =>
      __$FailedCopyWithImpl<_Failed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() registering,
    required TResult Function(AuthFailure failure) failed,
    required TResult Function() succeed,
  }) {
    return failed(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? registering,
    TResult Function(AuthFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Registering value) registering,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) {
    return failed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Registering value)? registering,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed(this);
    }
    return orElse();
  }
}

abstract class _Failed implements RegisterAccountState {
  const factory _Failed(AuthFailure failure) = _$_Failed;

  AuthFailure get failure => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$FailedCopyWith<_Failed> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$SucceedCopyWith<$Res> {
  factory _$SucceedCopyWith(_Succeed value, $Res Function(_Succeed) then) =
      __$SucceedCopyWithImpl<$Res>;
}

/// @nodoc
class __$SucceedCopyWithImpl<$Res>
    extends _$RegisterAccountStateCopyWithImpl<$Res>
    implements _$SucceedCopyWith<$Res> {
  __$SucceedCopyWithImpl(_Succeed _value, $Res Function(_Succeed) _then)
      : super(_value, (v) => _then(v as _Succeed));

  @override
  _Succeed get _value => super._value as _Succeed;
}

/// @nodoc

class _$_Succeed implements _Succeed {
  const _$_Succeed();

  @override
  String toString() {
    return 'RegisterAccountState.succeed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Succeed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() registering,
    required TResult Function(AuthFailure failure) failed,
    required TResult Function() succeed,
  }) {
    return succeed();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? registering,
    TResult Function(AuthFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) {
    if (succeed != null) {
      return succeed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Registering value) registering,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) {
    return succeed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Registering value)? registering,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) {
    if (succeed != null) {
      return succeed(this);
    }
    return orElse();
  }
}

abstract class _Succeed implements RegisterAccountState {
  const factory _Succeed() = _$_Succeed;
}
