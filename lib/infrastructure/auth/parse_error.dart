import 'package:freezed_annotation/freezed_annotation.dart';

part 'parse_error.freezed.dart';
part 'parse_error.g.dart';

@freezed
class ParseError with _$ParseError {
  const factory ParseError({int? code, String? error}) = _ParseError;

  factory ParseError.fromJson(Map<String, dynamic> json) =>
      _$ParseErrorFromJson(json);
}
