import 'package:auto_route/auto_route.dart';
import 'blank_screen.dart';
import '../auth/login_screen.dart';
import '../auth/register_screen.dart';

@MaterialAutoRouter(routes: <AutoRoute>[
  MaterialRoute(page: BlankScreen, path: "/blank"),
  MaterialRoute(page: LoginScreen, initial: true, path: "/"),
  MaterialRoute(page: RegisterScreen, path: "/register"),
])
class $KKMRouter {}
